#!/usr/bin/env python3
from make_rap import *


def init():
    print(""" 
    Words List Ita | Lista Parole Italiano = words.txt 
    Words List UK  | Lista Parole Inglese = words_uk.txt 
    Words List USA | Lista Parole Americano = words_usa.txt 

    """)
    word_list_path = input(
        "Choose a words file | Scegli un file di parole (default: words_ita.txt)"

    )
    if word_list_path == "":
        word_list_path = "words_ita.txt"
    playing = True
    song = []

    while playing:
        write_song(song, word_list_path)
        save_song(song)

        keep_rapping = input("do you want to continue? (y/n)")
        if keep_rapping.lower() == "y":
            playing = True
        else:
            playing = False
            print("Goodbye! Ciao!")


init()
