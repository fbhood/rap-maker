#!/usr/bin/env python3.7
import subprocess


def find_ryme(bar, ryme, word_list_path):
    sl = bar.split(" ")
    word = sl[-1]
    w_slice = word[-ryme:] + "$"
    words = subprocess.run(["grep", w_slice, word_list_path])
    print(words)


def write_song(song, word_list_path):
    print(""" 
    EN: Write a bar here, the last word will be selected for the next ryme.
    ITA: Scrivi una riga qui. L'ultima parola viene selezionata per la prossima rima.
    """)
    bar = input("Add your bar here | Scrivi la prima riga: ")
    song.append(bar)
    sl = bar.split(" ")
    word = sl[-1]
    print(
        """ 
        Word Selected | Parola Selezionata: {word}
        
        """.format(word=word))
    ryme_characters = int(
        input("Number of characters from the end | Numero di lettere dalla fine (Insert a number: 3)"))
    find_ryme(bar, ryme_characters, word_list_path)

    print(""" 
    EN: Now look at the words list above and choose your ryming word.
    ITA: Ora guarda la lista di parole qui sopra e scegli una parola dall'elenco.
    """)
    select_ryme = input(
        "Type here the word you want to add to the ryme scheme: ")
    print(""" 
    EN: Write the rest of the bar.
    ITA: Scrivi il resto della riga.

    
    {}
    __________________________{}
    """.format(bar, select_ryme))
    next_bar = input(
        "write another bar excluding the ryme | Scrivi il testo prima della rima: ") + select_ryme
    song.append(next_bar)


def save_song(song):
    separator = "---------------RAP--------------------"
    print(separator)
    for line in song:
        print(line)
    print(separator)

    if len(song) % 4 == 0 or len(song) == 16:
        save = input("Song Completed, do you want to save it? (y/n)")
        if save.lower() == "y":

            with open(input("File name: (rap.txt)"), "w") as file:
                for line in song:
                    file.writelines(line+"\n")
